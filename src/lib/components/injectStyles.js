import { inject } from 'vue';

const injectStyles = (prefix, stylesMap = {}) => {
  const styles = {};
  Object.keys(stylesMap).forEach(suffix => {
    styles[suffix] = stylesMap[suffix] || inject(`${prefix}-${suffix}`);
  });
  return styles;
};

export default injectStyles;
