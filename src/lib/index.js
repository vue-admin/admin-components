import merge from 'lodash.merge';
import AdminPagination from './components/AdminPagination.vue';
import AdminTable from './components/AdminTable.vue';
import AdminFooter from './components/AdminFooter.vue';
import AdminLayout from './components/AdminLayout.vue';
import AdminMenu from './components/AdminMenu.vue';
import AdminMenuItem from './components/AdminMenuItem.vue';
import AdminNavbar from './components/AdminNavbar.vue';
import PageSizeChanger from './components/PageSizeChanger.vue';

const defaultOptions = {
  'admin-navbar': {
    component: AdminNavbar,
    classes: {
      main: 'admin-navbar-main',
      brand: 'admin-navbar-brand',
      toggler: 'admin-navbar-toggler',
      menuWrapper: 'admin-navbar-menuWrapper',
      menuLeft: 'admin-navbar-menuLeft',
      menuRight: 'admin-navbar-menuRight',
    },
  },
  'admin-layout': {
    component: AdminLayout,
    classes: {
      layout: 'admin-layout-layout',
      main: 'admin-layout-main',
      menu: 'admin-layout-menu',
    },
  },
  'admin-footer': {
    component: AdminFooter,
    classes: {
      main: 'admin-footer-main',
    },
  },
  'admin-menu': {
    component: AdminMenu,
    classes: {
      wrapper: 'admin-menu-wrapper',
      itemWrapper: 'admin-menu-itemWrapper',
      item: 'admin-menu-item',
    },
  },
  'admin-page-size-changer': PageSizeChanger,
  'admin-menu-item': AdminMenuItem,
  'admin-table': {
    component: AdminTable,
    classes: {
      wrapper: 'admin-table-wrapper',
      main: 'admin-table-main',
      head: 'admin-table-head',
      th: 'admin-table-th',
      td: 'admin-table-td',
      tr: 'admin-table-tr',
      actionsWrapper: 'admin-table-actionsWrapper',
    },
  },
  'admin-pagination': {
    component: AdminPagination,
    classes: {
      wrapper: 'admin-pagination-wrapper',
      selectorWrapper: 'admin-pagination-selectorWrapper',
      selector: 'admin-pagination-selector',
      pagesWrapper: 'admin-pagination-pagesWrapper',
      itemWrapper: 'admin-pagination-itemWrapper',
      item: 'admin-pagination-item',
      activeItem: 'admin-pagination-activeItem',
    },
  },
};

const AdminComponents = {
  install: (app, extraOptions = {}) => {
    const options = merge({}, defaultOptions, extraOptions);

    Object.keys(options).forEach(key => {
      if (options[key].component) {
        app.component(key, options[key].component);
      } else {
        app.component(key, options[key]);
      }

      if (options[key].classes) {
        Object.keys(options[key].classes).forEach(classKey => {
          app.provide(`${key}-${classKey}`, options[key].classes[classKey]);
        });
      }
    });
  },
};
export default AdminComponents;
