import { createApp } from 'vue';
import App from './App.vue';
import AdminComponents from './lib';
import './main.css';

createApp(App)
  .use(AdminComponents, {
    /* 'admin-table': {
      classes: {
        main: 'table table-sm',
      },
    },
    'admin-pagination': {
      classes: {
        wrapper: 'pagination pagination-xl justify-content-end',
      },
    }, */
  })
  .mount('#app');
