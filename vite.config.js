const path = require('path')
import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    outDir: 'umd',
    lib: {
      formats: [ 'umd' ],
      name: 'AdminComponents',
      fileName: 'index',
      entry: path.resolve(__dirname, 'src/lib/index.js'),
    },
    rollupOptions: {
      external: [ 'vue' ],
      output: {
        globals: {
          vue: 'Vue'
        }
      }
    }
  }
});
