module.exports = {
  root: true,
  env: {
    node: true,
    "vue/setup-compiler-macros": true,
  },
  extends: [
    'plugin:vue/vue3-strongly-recommended',
    '@vue/airbnb',
  ],
  parserOptions: {
    ecmaVersion: 6,
    requireConfigFile: false,
    parser: '@babel/eslint-parser',
  },
  rules: {
    'import/no-extraneous-dependencies': [ 'error', { devDependencies: true } ],
    'no-param-reassign': [
      'error', {
        'props': true,
        'ignorePropertyModificationsFor': [ 'target', 'container' ],
      },
    ],
    'arrow-parens': [ 'error', 'as-needed' ],
    'array-bracket-newline': [
      'error', {
        multiline: true,
        minItems: 3,
      },
    ],
    'array-bracket-spacing': [ 'error', 'always' ],
    'quote-props': [ 'error', 'consistent' ],
    'block-spacing': [ 'error', 'never' ],
    'comma-spacing': [
      'error', {
        before: false,
        after: true,
      },
    ],
    'comma-dangle': [
      'error', {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'never',
        functions: 'never',
      },
    ],
    'max-len': [
      'error',
      {
        code: 120,
        ignoreComments: true,
        ignoreUrls: true,
      },
    ],
    'import/extensions': [
      'error',
      'always',
      {
        js: 'never',
        vue: 'always',
      },
    ],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
  },
};
